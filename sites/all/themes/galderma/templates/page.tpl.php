<?php
/**
 * @file
 * Returns the HTML for a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728148
 */
?>


<div class="site-header">
	<a class="site-header__logo" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
		<img src="/sites/all/themes/galderma/images/site-header/logo.png" width="417" height="46" alt="<?php print t('Home'); ?>" />
	</a>

	<?php if (!empty($site_slogan)): ?>
		<div class="site-header__slogan">
				<?php print $site_slogan; ?>
		</div>
	<?php endif; ?>
</div>

<div class="site-menu">
	<?php if (!empty($page['navigation'])): ?>
		<?php print render($page['navigation']); ?>
	<?php endif; ?>
</div>

<?php print render($page['content']); ?>

<?php /* ?>

<header id="navbar" role="banner" class="<?php print $navbar_classes; ?>">
	<div class="<?php print $container_class; ?>">
		<div class="navbar-header">
			<?php if ($logo): ?>
				<a class="logo navbar-btn pull-left" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
					<img src="/sites/all/themes/galderma/images/logo.png" alt="<?php print t('Home'); ?>" />
				</a>
			<?php endif; ?>

			<?php if (!empty($site_slogan)): ?>
				<p class="lead"><?php print $site_slogan; ?></p>
			<?php endif; ?>

			<?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only"><?php print t('Toggle navigation'); ?></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			<?php endif; ?>
		</div>

		<?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
			<div class="navbar-collapse collapse">
				<nav role="navigation">
					<?php if (!empty($primary_nav)): ?>
						<?php print render($primary_nav); ?>
					<?php endif; ?>
					<?php if (!empty($secondary_nav)): ?>
						<?php print render($secondary_nav); ?>
					<?php endif; ?>
					<?php if (!empty($page['navigation'])): ?>
						<?php print render($page['navigation']); ?>
					<?php endif; ?>
				</nav>
			</div>
		<?php endif; ?>
	</div>

	<div class="site-menu">
		<div class="<?php print $container_class; ?>">
			<?php if (!empty($page['navigation'])): ?>
				<?php print render($page['navigation']); ?>
			<?php endif; ?>
		</div>
	</div>

</header>

<div class="main-container <?php print $container_class; ?>">

	<header role="banner" id="page-header">
		<?php print render($page['header']); ?>
	</header> <!-- /#page-header -->

	<div class="row">

		<?php if (!empty($page['sidebar_first'])): ?>
			<aside class="col-sm-3" role="complementary">
				<?php print render($page['sidebar_first']); ?>
			</aside>  <!-- /#sidebar-first -->
		<?php endif; ?>

		<section<?php print $content_column_class; ?>>
			<?php if (!empty($page['highlighted'])): ?>
				<div class="highlighted jumbotron"><?php print render($page['highlighted']); ?></div>
			<?php endif; ?>
			<?php if (!empty($breadcrumb)): print $breadcrumb; endif;?>
			<a id="main-content"></a>
			<?php print render($title_prefix); ?>
			<?php if (!empty($title)): ?>
				<h1 class="page-header"><?php print $title; ?></h1>
			<?php endif; ?>
			<?php print render($title_suffix); ?>
			<?php print $messages; ?>
			<?php if (!empty($tabs)): ?>
				<?php print render($tabs); ?>
			<?php endif; ?>
			<?php if (!empty($page['help'])): ?>
				<?php print render($page['help']); ?>
			<?php endif; ?>
			<?php if (!empty($action_links)): ?>
				<ul class="action-links"><?php print render($action_links); ?></ul>
			<?php endif; ?>
			<?php print render($page['content']); ?>
		</section>

		<?php if (!empty($page['sidebar_second'])): ?>
			<aside class="col-sm-3" role="complementary">
				<?php print render($page['sidebar_second']); ?>
			</aside>  <!-- /#sidebar-second -->
		<?php endif; ?>

	</div>
</div>

<?php if (!empty($page['footer'])): ?>
	<footer class="footer <?php print $container_class; ?>">
		<?php print render($page['footer']); ?>
	</footer>
<?php endif; ?>

<?php */ ?>