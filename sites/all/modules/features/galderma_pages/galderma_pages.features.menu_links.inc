<?php
/**
 * @file
 * galderma_pages.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function galderma_pages_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_about-rosacea:node/1.
  $menu_links['main-menu_about-rosacea:node/1'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/1',
    'router_path' => 'node/%',
    'link_title' => 'About Rosacea',
    'options' => array(
      'identifier' => 'main-menu_about-rosacea:node/1',
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: main-menu_additional-resources:node/8.
  $menu_links['main-menu_additional-resources:node/8'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/8',
    'router_path' => 'node/%',
    'link_title' => 'Additional Resources',
    'options' => array(
      'identifier' => 'main-menu_additional-resources:node/8',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'main-menu_support:node/3',
  );
  // Exported menu link: main-menu_gallery:node/9.
  $menu_links['main-menu_gallery:node/9'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/9',
    'router_path' => 'node/%',
    'link_title' => 'Gallery',
    'options' => array(
      'identifier' => 'main-menu_gallery:node/9',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_support:node/3',
  );
  // Exported menu link: main-menu_home:<front>.
  $menu_links['main-menu_home:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Home',
    'options' => array(
      'identifier' => 'main-menu_home:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: main-menu_lifestyle-tips:node/4.
  $menu_links['main-menu_lifestyle-tips:node/4'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/4',
    'router_path' => 'node/%',
    'link_title' => 'Lifestyle Tips',
    'options' => array(
      'identifier' => 'main-menu_lifestyle-tips:node/4',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_support:node/3',
  );
  // Exported menu link: main-menu_overview:node/5.
  $menu_links['main-menu_overview:node/5'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/5',
    'router_path' => 'node/%',
    'link_title' => 'Overview',
    'options' => array(
      'identifier' => 'main-menu_overview:node/5',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'main-menu_about-rosacea:node/1',
  );
  // Exported menu link: main-menu_support:node/3.
  $menu_links['main-menu_support:node/3'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/3',
    'router_path' => 'node/%',
    'link_title' => 'Support',
    'options' => array(
      'identifier' => 'main-menu_support:node/3',
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: main-menu_symptoms:node/6.
  $menu_links['main-menu_symptoms:node/6'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/6',
    'router_path' => 'node/%',
    'link_title' => 'Symptoms',
    'options' => array(
      'identifier' => 'main-menu_symptoms:node/6',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'main-menu_about-rosacea:node/1',
  );
  // Exported menu link: main-menu_triggers:node/7.
  $menu_links['main-menu_triggers:node/7'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/7',
    'router_path' => 'node/%',
    'link_title' => 'Triggers',
    'options' => array(
      'identifier' => 'main-menu_triggers:node/7',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'main-menu_about-rosacea:node/1',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('About Rosacea');
  t('Additional Resources');
  t('Gallery');
  t('Home');
  t('Lifestyle Tips');
  t('Overview');
  t('Support');
  t('Symptoms');
  t('Triggers');

  return $menu_links;
}
