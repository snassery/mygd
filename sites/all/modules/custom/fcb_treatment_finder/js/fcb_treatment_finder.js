(function ($) {
  Drupal.behaviors.fcb_treatment_finder = {
    attach: function(context, settings) {
      $.each(Drupal.settings.images, function( key, value ) {
        if (key === 'ages') {
          manage_treament_form(key);
        }
        if (value.length !== 0) {
          $('.'+key + " .form-type-radio label" ).each(function( index, val ) {
            $(this).text('');
            $(this).css('background-image', 'url(' + value[index] + ')');
          });
        }
        $('#next-'+key).click(function() {
          manage_treament_form(key);
        });
        $('#back-'+key).click(function() {
          manage_treament_form(key);
        });
      });

      function manage_treament_form(key) {
        $('#fcb-treatments-finder-form fieldset').each(function() {
          $(this).hide();
        });
        $('.treatment-'+key).show();
      }
    }
  }
})(jQuery);
