<?php if($title): ?>
<aside id="isi">
  <?php if($title) ?>
  <div class='title'>
    <?php print $title; ?>

  </div>

  <div class="content">
    <?php print $body; ?>

  </div>
</aside>
<?php else: ?>
  <div class="isi-content">
   <?php print $body; ?>
 </div>
<?php endif; ?>
