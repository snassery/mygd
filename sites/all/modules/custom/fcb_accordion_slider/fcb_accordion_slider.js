(function ($) {
  $(function () {
    $('.accordion-slider').on('click', '.as-panel', function (event) {
      event.preventDefault();

      $('.as-panel', event.delegateTarget).removeClass('is-active').addClass('is-collapsed');
      $(this).addClass('is-active').removeClass('is-collapsed');
      $(event.delegateTarget).addClass('has-active');
    });
  });
})(jQuery);