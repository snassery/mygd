<div class="accordion-slider">
	<div class="as-panels">
		<?php foreach ($slides as $slide) { ?>
			<div class="as-panel">
				<img class="as-background" src="<?php echo $slide['pic']; ?>" alt="<?php echo htmlspecialchars($slide['title']); ?>"/>
				<img class="as-logo" src="<?php echo $slide['logo']; ?>" alt="<?php echo htmlspecialchars($slide['title']); ?>"/>
				<img class="as-btn" src="<?php echo $slide['btn']; ?>" alt="<?php echo htmlspecialchars($slide['title']); ?>"/>
				<a href="<?php echo $slide['url']; ?>"><img class="as-big-btn" src="<?php echo $slide['big_btn']; ?>" alt="<?php echo htmlspecialchars($slide['title']); ?>" /></a>
				<div class="as-title"><?php echo $slide['title']; ?></div>
				<div class="as-text"><?php echo $slide['text']; ?></div>
				<div class="as-caption"><?php echo $slide['caption']; ?></div>
			</div>
		<?php } ?>
	</div>
</div>